import os
import re
import string
from time import sleep

import pandas as pd
import requests
from bs4 import BeautifulSoup
from requests.structures import CaseInsensitiveDict
from tqdm import tqdm

os.environ["CURL_CA_BUNDLE"] = "/etc/ssl/certs/ca-certificates.crt"
remove_tags = ["span"]
# pylint: disable=no-name-in-module
from lxml.html.clean import Cleaner  # nosec

cleaner = Cleaner(
    remove_tags=remove_tags,
    style=True,
    safe_attrs_only=True,
    safe_attrs=["name"],
)


def query_by_letter(letter="A"):
    data = (
        "page=1"
        "&affliste=0"
        "&affNumero=0"
        "&isAlphabet=1"
        "&inClauseSubst=0"
        "&nomSubstances="
        "&typeRecherche=0"
        "&choixRecherche=medicament"
        "&paginationUsed=0"
        "&txtCaracteres=%s"
        "&btnMedic.x=9"
        "&btnMedic.y=15"
        "&radLibelle=2"
        "&txtCaracteresSub="
        "&radLibelleSub=4"
        "&txtCaracteresPath="
        "&radLibellePath=6" % letter
    )
    resp = query_bdpm(data)
    return resp.text


def query_bdpm(
    data, url="https://base-donnees-publique.medicaments.gouv.fr/index.php"
):
    headers = CaseInsensitiveDict()
    headers["Content-Type"] = "application/x-www-form-urlencoded"

    resp = requests.post(url, headers=headers, data=data)
    return resp


def extract_cis_codes(response):
    soup = BeautifulSoup(response, features="html.parser")
    links = soup.findAll("a", {"class": "standart"})
    cis_codes = [l.attrs["href"].split("=")[1] for l in links]

    # assert len(cis_codes) == len(links)
    return cis_codes


def query_rcp(cis_code, delay=0.100):
    sleep(delay)
    rcp_url = f"https://base-donnees-publique.medicaments.gouv.fr/affichageDoc.php?specid={cis_code}&typedoc=R"
    resp = requests.get(rcp_url)

    return resp.text


def get_pathologies(
    url="https://base-donnees-publique.medicaments.gouv.fr/pathologies.php",
):
    df_pathologies = pd.read_html(url)[0]
    pathologies = df_pathologies["Nom de la pathologie"].values
    return pathologies


def query_by_pathology(pathology):
    data = (
        "page=1"
        "&affliste=0"
        "&affNumero=0"
        "&isAlphabet=1"
        "&inClauseSubst=0"
        "&nomSubstances="
        "&typeRecherche=2"
        "&choixRecherche=pathologie"
        "&paginationUsed=0"
        "&txtCaracteres="
        "&btnMedic.x=15"
        "&btnMedic.y=9"
        "&radLibelle=2"
        "&txtCaracteresSub="
        "&radLibelleSub=4"
        "&txtCaracteresPath=%s"
        "&radLibellePath=6" % pathology
    )
    resp = query_bdpm(data)
    return resp.text


def extract_rcp_html(response):
    soup = BeautifulSoup(response, features="html.parser")
    doc = soup.find(id="textDocument")
    if not doc:
        raise Exception("Did not find RCP")
    return doc


go_up_regex = re.compile(r'<a href="#Haut.*?</a>')


def clean_rcp_html(rcp_html):
    """Removes unused formatting from the HTML tags"""
    # Removes go up related tags
    rcp_html = str(rcp_html).replace("\xa0", "")
    rcp_html = go_up_regex.sub("", rcp_html)
    html_clean = cleaner.clean_html(rcp_html)
    return html_clean


def obtain_medicament_RCP():
    letters = string.ascii_uppercase
    cis_rcp_dict = {}
    for letter in tqdm(letters):
        docs_response = query_by_letter(letter)
        cis_codes = extract_cis_codes(docs_response)
        for cis_code in tqdm(cis_codes[:]):
            try:
                rcp_page_response = query_rcp(cis_code)
                rcp_html = extract_rcp_html(rcp_page_response)
                rcp_html = clean_rcp_html(rcp_html)
                rcp_html = f"<!DOCTYPE html>\n<html>\n<body>{rcp_html}\n</body>\n</html>"
                cis_rcp_dict[cis_code] = rcp_html

            except:
                tqdm.write(f"Could not get RCP of this CIS {cis_code}.")
                cis_rcp_dict[cis_code] = ""
    df = pd.DataFrame(
        cis_rcp_dict.values(),
        index=cis_rcp_dict.keys(),
        columns=["RCP_html"],
    )
    df.index = df.index.rename("Code_CIS")
    # We use excel bc HTML gets all tangled up with new lines, etc. :(
    df.to_excel("./data/raw/CIS_RCP.xlsx")
    # Save sample
    df.head(10).to_excel("./data/raw/CIS_RCP_sample.xlsx")


def obtain_medicament_pathology():
    pathologies_list = get_pathologies()
    cis_pathology = []
    for patho in tqdm(pathologies_list):
        docs_response = query_by_pathology(patho)
        cis_codes = extract_cis_codes(docs_response)
        cis_pathology.extend(list(zip(cis_codes, [patho] * len(cis_codes))))
    cis_pathology = sorted(cis_pathology, key=lambda x: x[0])
    df = pd.DataFrame(cis_pathology, columns=["Code_CIS", "Pathologie"])
    df.to_csv("./data/raw/CIS_Pathologie.csv", sep="\t", index=False)


if __name__ == "__main__":
    obtain_medicament_RCP()
    obtain_medicament_pathology()
