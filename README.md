# defi-idoc-data

Dans le cadre du Défi iDoc Santé, ce dépôt contient des codes ayant permi de :
- scrapper les publications de l'INCA https://www.data.gouv.fr/fr/datasets/publications-de-linca/
- créer une version enrichie de la BDPM  https://www.data.gouv.fr/fr/datasets/base-de-donnees-publique-des-medicaments-defi-idoc-sante/
