#! /usr/bin/env python
import click
from dotenv import load_dotenv

from defi_idoc_data.utils import hello

# see `.env` for requisite environment variables
load_dotenv()


@click.group()
def cli():
    pass


@cli.command()
@click.version_option(package_name="defi-idoc-data")
def main() -> None:
    """defi-idoc-data Main entrypoint"""
    click.secho(hello(), fg="green")


if __name__ == "__main__":
    cli()
